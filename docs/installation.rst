.. image:: https://img.shields.io/badge/python-3.10-blue
   :alt: Python version badge

.. image:: https://img.shields.io/badge/PostgreSQL-16.0-yellow
   :target: https://www.postgresql.org/

.. image:: https://img.shields.io/badge/GEOS-3.9.0-red
   :target: https://trac.osgeo.org/geos/

Installation
------------

Local Development
*****************

*  Clone this repo locally

.. code-block:: console

    $ git clone git@gitlab.com:GKyriak/simurex.git

or

.. code-block:: console

    $ git clone https://gitlab.com/GKyriak/simurex.git

*  Create the environment:

Open a command line tool in the uhi_intensity root folder, enter the
``ci`` folder, and then run in the terminal
the following :

.. code-block:: console

    $ conda env create -f environment.yml

To remove an existing environment:

.. code-block:: console

    $ conda remove --name uwg_simurex --all


Please refer to the ``../../requirements.txt`` file for detailed package requirements.

* Activate the uwg_simurex:

.. code-block:: console

    $ conda activate uwg_simurex


