# Atelier Urban Weather Generator (UWG)

Cet atelier est consacré au processus complexe de génération de fichiers météorologiques prenant en compte l’effet d’îlot de chaleur urbain.
Il se penche sur l’utilisation du code de calcul de micro-modélisation 2.5D « Urban Weather Generator » qui permet un de générer un fichier météorologique urbain à partir
- de données météorologiques aéroportuaires 
- des caractéristiques géométriques et thermiques d’un quartier. 

Les participants exploreront non seulement les principes physiques fondamentaux, mais aussi les subtilités de chaque sous-module de l'outil.
En outre, l’atelier fournit des informations approfondies sur les données d’entrée requises et propose des méthodes efficaces pour obtenir ces données. Grâce à des démonstrations pratiques, les participants acquerront une compréhension des calculs de base du modèle. L'objectif est de donner aux participants les connaissances et les compétences nécessaires pour générer des fichiers météorologiques urbain.


# I. Installation du dépôt

> Pré-requis : avoir installé Anaconda et GIT sur son PC. Dans un invite de commance vérifier que les commandes `conda`, `pip` et `git` sont connues

## I.1 Récupération des fichiers vie un dépôt git

Pour cloner ce répertoire GitLab, vous pouvez suivre ces étapes générales :

* Copiez l'URL du dépôt : https://gitlab.com/GKyriak/simurex
* Ouvrez votre terminal ou Git Bash.
* Déplacez-vous vers l'emplacement où vous souhaitez cloner le répertoire.
* Utilisez la commande git clone suivie de l'URL copiée pour cloner le dépôt.

```
git clone https://gitlab.com/GKyriak/simurex

```
## I.1 Création d'un environnement conda
Dans cette étape nous allons préparer un environnemt `conda` afin d'exécuter le code
* à l'endroit où a été cloné le dossier, ouvrir un exécuteur de commande. 
* taper cette commande `conda env create -f environment.yml`

## I.2 Création d'un kernel pour jupyter
Dans cette étape nous allons préparer l'application jupyter notebook en créant un "kernel" qui permettra d'utiliser le bon environnement python
* à l'endroit où a été cloné le dossier, ouvrir un exécuteur de commande
* activer l'environnement installé `conda activate uwg_simurex`
* exécuter la commande suivante : `python -m ipykernel install --user --name=uwg_simurex`


## II. Lancement des Notebooks Jupyter
* Ouvrir un environnement Jupyter notebook, soit par Anaconda, soit par l'invite de commande en tapant `jupyter notebook`
* Ouvrir le notebook `AtelierSimurex_UrbanWeatherGenerator.ipynb`


