import json

import matplotlib.pyplot as plt
import pandas as pd
# from shapely.geometry import box
import numpy as np
import os
import glob
import geopandas as gpd
from uwg import UWG



def epw_columns():
    liste_col = ['Year', 'Month', 'Day', 'Hour', 'Minute',
       'Data Source and Uncertainty Flags', 'Dry Bulb Temperature',
       'Dew Point Temperature', 'Relative Humidity',
       'Atmospheric Station Pressure', 'Extraterrestrial Horizontal Radiation',
       'Extraterrestrial Direct Normal Radiation',
       'Horizontal Infrared Radiation Intensity',
       'Global Horizontal Radiation', 'Direct Normal Radiation',
       'Diffuse Horizontal Radiation', 'Global Horizontal Illuminance',
       'Direct Normal Illuminance', 'Diffuse Horizontal Illuminance',
       'Zenith Luminance', 'Wind Direction', 'Wind Speed', 'Total Sky Cover',
       'Opaque Sky Cover (used if Horizontal IR Intensity missing)',
       'Visibility', 'Ceiling Height', 'Present Weather Observation',
       'Present Weather Codes', 'Precipitable Water', 'Aerosol Optical Depth',
       'Snow Depth', 'Days Since Last Snowfall', 'Albedo',
       'Liquid Precipitation Depth', 'Liquid Precipitation Quantity']

    return liste_col

def calculate_modified_perimeter(row: gpd.GeoSeries, bld: gpd.GeoDataFrame):
    """
    Calculate the initial and modified weighted perimeter for a building
    after removing adjacent surfaces.

    Parameters:
    - row (Series): A row from the GeoDataFrame representing a building.
    - bld (GeoDataFrame): The GeoDataFrame containing all buildings.

    Returns:
    - initial_w_perimeter (float): The initial weighted perimeter of the building.
    - w_perimeter (float): The modified weighted perimeter of the building.
    """

    # Extract building geometry from the row
    building_geometry = row['geometry']

    # Find shared edges with other bld
    shared_edges = bld['geometry'].intersection(building_geometry)

    # Calculate initial weighted perimeter based on building height
    initial_w_perimeter = building_geometry.length * row.height
    intersected_perimeter = 0

    # Calculate modified perimeter by considering shared edges
    for idx, intersection in enumerate(shared_edges):
        if intersection.geom_type == 'LineString' or intersection.geom_type == 'MultiLineString':
            intersected_height = min(bld.iloc[idx].height, row.height)
            intersected_perimeter += intersection.length * intersected_height
    w_perimeter = initial_w_perimeter - intersected_perimeter

    return w_perimeter




if __name__ == "__main__":
    print('Hello world')